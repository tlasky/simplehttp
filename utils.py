import zlib
from typing import *


def raw_generator(fileobj, max_length: int = 512) -> Generator[bytes, None, None]:
    while data := fileobj.read(max_length):
        yield data
    return


def zlib_iterator(iterable: Iterable[bytes], wbits: int) -> Iterable[bytes]:
    compressor = zlib.compressobj(wbits=wbits)
    for data in iterable:
        if compressed := compressor.compress(data):
            yield compressed
    yield compressor.flush()
    return


def gzip_iterator(iterable: Iterable[bytes]) -> Iterable[bytes]:
    return zlib_iterator(iterable, 31)


def deflate_iterator(iterable: Iterable[bytes]) -> Iterable[bytes]:
    return zlib_iterator(iterable, 15)


if __name__ == '__main__':
    import io

    funcs = [
        raw_generator,

        gzip_iterator,
        lambda x: gzip_iterator(raw_generator(x, 10)),

        deflate_iterator,

        lambda x: gzip_iterator(deflate_iterator(x)),
        lambda x: deflate_iterator(gzip_iterator(x))
    ]

    file = io.BytesIO(b'Hello World!\n' * 50)

    for f in funcs:
        file.seek(0)
        print(list(f(file)))
