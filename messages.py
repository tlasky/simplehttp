import io
from os import fstat
from typing import *
from io import BytesIO
from magic import Magic
from http import HTTPStatus
from datetime import datetime
from traceback import format_exception
from urllib.parse import urlparse, ParseResult, parse_qsl

from utils import raw_generator, gzip_iterator

magic = Magic(mime=True, mime_encoding=True)


class Headers:
    def __init__(self):
        self._headers: Dict[str, str] = dict()

    def __dict__(self):
        return self._headers

    def __iter__(self):
        for name, value in self._headers.items():
            yield name, value

    def __repr__(self):
        return f'{self.__class__.__name__}({", ".join(f"{name}={value}" for name, value in self)})'

    @staticmethod
    def _norm_name(name: str) -> str:
        return '-'.join([word.strip().capitalize() for word in name.split('-')])

    def set(self, name: str, value: Any):
        name = self._norm_name(name)
        if value is not None and str(value).strip():
            self._headers[name] = str(value)
        else:
            if name in self._headers:
                self._headers.pop(name)

    def get(self, name: str, default: Any = str()) -> Any:
        return self._headers.get(self._norm_name(name), default)

    def update(self, headers: Dict[str, str]):
        for name, value in headers.items():
            self.set(name, value)


class Message:
    def __init__(self):
        self.http_version = '1.1'
        self.headers = Headers()
        self.body = BytesIO()

    def to_dict(self):
        d = self.__dict__
        for attr_name in dir(self):
            attr = getattr(self, attr_name)
            if isinstance(attr, property):
                d[attr_name] = attr
        return d

    @property
    def keep_alive(self) -> bool:
        return not self.headers.get('connection', str()).lower() == 'close'

    def __repr__(self):
        return f'{self.__class__.__name__}({", ".join([f"{name}={value}" for name, value in self.to_dict().items()])})'


class Request(Message):
    def __init__(self, remote_addr: Tuple[str, int] = tuple()):
        super().__init__()

        self.remote_addr = remote_addr
        self.method = str()
        self.raw_url = str()
        self.finished = False

    @property
    def url(self) -> ParseResult:
        return urlparse(self.raw_url)

    @url.setter
    def url(self, value: str):
        self.raw_url = value

    @property
    def query(self) -> dict:
        return dict(parse_qsl(self.url.query))

    @property
    def accept_encoding(self) -> List[str]:
        if accept_header := self.headers.get('accept-encoding'):
            return [encoding.strip() for encoding in accept_header.split(',')]
        return list()


class BaseResponse(Message):
    def __init__(self):
        super().__init__()

        self.status_code = 200

    @property
    def status(self) -> HTTPStatus:
        return HTTPStatus(self.status_code)

    def get_content_length(self) -> int:
        if type(self.body) is BytesIO:
            return self.body.getbuffer().nbytes

        self.body.seek(0)
        return len(self.body.read())

    def get_content_type(self) -> str:
        return 'text/plain'

    def format_status(self) -> bytes:
        return f'HTTP/{self.http_version} {self.status.value} {self.status.phrase}\r\n'.encode()

    def format_headers(self) -> bytes:
        return ''.join(f'{name}: {value}\r\n' for name, value in self.headers).encode() + b'\r\n'


class TextResponse(BaseResponse):
    def __init__(self, data: bytes = None):
        super().__init__()

        if data:
            self.body.write(data)

    def get_content_type(self) -> str:
        self.body.seek(0)
        return magic.from_buffer(self.body.read(2048))


class ErrorResponse(TextResponse):
    def __init__(self, code: int = 500):
        super().__init__()

        self.status_code = code
        self.body.write(f'{self.status.value} {self.status.phrase} - {self.status.description}'.encode())

    def write_exception(self, exc: Exception):
        self.body.write(b'\n' * 2)
        self.body.write(''.join(format_exception(exc.__class__, exc, exc.__traceback__)).encode())


class FileResponse(BaseResponse):
    def __init__(self, file):
        super().__init__()

        self.body = file

    def get_content_length(self) -> int:
        return fstat(self.body.fileno()).st_size

    def get_content_type(self) -> str:
        return magic.from_file(self.body.name)


def set_request_headers(response: BaseResponse, request: Request):
    request_headers = ['connection']
    for name in request_headers:
        if not response.headers.get(name) and (value := request.headers.get(name)):
            response.headers.set(name, value)


def set_default_headers(response: BaseResponse):
    default_headers = {
        'content-length': str(response.get_content_length()),
        'content-type': response.get_content_type(),
        'server': 'tlasky test server',
        'date': datetime.utcnow().strftime('%a, %d %b %Y %H:%M:%S GMT')
    }
    for name, value in default_headers.items():
        if not response.headers.get(name) and value:
            response.headers.set(name, value)


class ResponseStream:
    def __init__(self, response: BaseResponse):
        self.response = response
        self.chunk_size = 512

    def stream(self) -> Generator[bytes, None, None]:
        yield self.response.format_status()
        yield self.response.format_headers()

        self.response.body.seek(0)
        for chunk in raw_generator(self.response.body, self.chunk_size):
            yield chunk
        return


class GzipResponseStream(ResponseStream):
    def __init__(self, response: BaseResponse):
        super().__init__(response)

        self.gzip_body = io.BytesIO()

        self.response.body.seek(0)
        for chunk in gzip_iterator(raw_generator(self.response.body, self.chunk_size)):
            self.gzip_body.write(chunk)

        self.response.headers.set('content-length', self.gzip_body.getbuffer().nbytes)
        self.response.headers.set('content-encoding', 'gzip')

    def stream(self) -> Generator[bytes, None, None]:
        yield self.response.format_status()
        yield self.response.format_headers()

        self.gzip_body.seek(0)
        for chunk in raw_generator(self.gzip_body, self.chunk_size):
            yield chunk
        return


class ChunkedResponseStream(ResponseStream):
    def __init__(self, response: BaseResponse):
        super().__init__(response)

        self.response.headers.set('transfer-encoding', 'chunked')

    def stream(self) -> Generator[bytes, None, None]:
        yield self.response.format_status()
        yield self.response.format_headers()

        self.response.body.seek(0)
        for chunk in raw_generator(self.response.body, self.chunk_size):
            yield f'{len(chunk):X}\r\n'.encode()
            yield chunk + b'\r\n'

        yield b'0\r\n\r\n'
        return


class GzipChunkedResponseStream(ChunkedResponseStream):
    def __init__(self, response: BaseResponse):
        super().__init__(response)

        self.response.headers.set('content-encoding', 'gzip')

    def stream(self) -> Generator[bytes, None, None]:
        yield self.response.format_status()
        yield self.response.format_headers()

        self.response.body.seek(0)
        for chunk in gzip_iterator(raw_generator(self.response.body, self.chunk_size)):
            yield f'{len(chunk):X}\r\n'.encode()
            yield chunk + b'\r\n'

        yield b'0\r\n\r\n'
        return


def streamer_selector(response: BaseResponse, request: Request) -> Type[ResponseStream]:
    length = response.get_content_length()
    if 'gzip' in request.accept_encoding:
        if length is None or length > 1024 * 1000:
            return GzipChunkedResponseStream
        return GzipResponseStream
    if length is None:
        return ChunkedResponseStream
    return ResponseStream


if __name__ == '__main__':
    r = TextResponse('👽Hello World!👽'.encode() * 50)
    set_default_headers(r)

    r.body.seek(0)
    stream = ResponseStream(r).stream()
    while data := next(stream, None):
        print(data.decode(errors='backslashreplace'), end='')

    print()
    print('-' * 50)
    print()

    r.body.seek(0)
    stream = GzipResponseStream(r).stream()
    while data := next(stream, None):
        print(data.decode(errors='backslashreplace'), end='')

    print()
    print('-' * 50)
    print()

    r.body.seek(0)
    stream = ChunkedResponseStream(r).stream()
    while data := next(stream, None):
        print(data.decode(errors='backslashreplace'), end='')

    print()
    print('-' * 50)
    print()

    r.body.seek(0)
    stream = GzipChunkedResponseStream(r).stream()
    while data := next(stream, None):
        print(data.decode(errors='backslashreplace'), end='')
