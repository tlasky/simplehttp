import socket
import selectors
from typing import *
from pathlib import Path
from traceback import print_exc
from httptools import HttpRequestParser, HttpParserError

from messages import (
    Request,
    set_default_headers, set_request_headers, streamer_selector,
    BaseResponse, TextResponse, ErrorResponse, FileResponse
)

HOST = '0.0.0.0'
PORT = 8080
ADDR = (HOST, PORT)


class RequestParserProtocol:
    def __init__(self, data: 'SocketData'):
        self.data = data

    def on_message_complete(self):
        self.data.request.finished = True

    def on_header(self, name: bytes, value: bytes):
        self.data.request.headers.set(name.decode(), value.decode())

    def on_headers_complete(self):
        self.data.request.http_version = self.data.request_parser.get_http_version()
        self.data.request.method = self.data.request_parser.get_method().decode().upper()

        if host := self.data.request.headers.get('host'):  # TODO: This is not a right way.
            self.data.request.raw_url = f'http://{host}{self.data.request.raw_url}'

    def on_body(self, data: bytes):
        self.data.request.body.write(data)

    def on_url(self, data: bytes):
        self.data.request.url = data.decode()


class SocketData:
    def __init__(self, addr: Tuple[str, int]):
        self.addr = addr

        self.request_data = bytes()  # Just for debugging
        self.request_parser = HttpRequestParser(RequestParserProtocol(self))
        self.request = Request(self.addr)
        self.response: Union[None, BaseResponse] = None
        self.response_stream: Union[Generator[bytes, None, None], None] = None

    def clear(self):
        self.request_data = bytes()
        self.request_parser = HttpRequestParser(RequestParserProtocol(self))
        self.request = Request(self.addr)
        self.response = None
        self.response_stream = None


class Server:
    def __init__(self):
        self.running = False
        self.selector = selectors.DefaultSelector()
        self.listener_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.listener_sock.bind(ADDR)
        self.listener_sock.listen()
        self.listener_sock.setblocking(False)
        print('\t* Listening on:', ADDR)
        self.selector.register(self.listener_sock, selectors.EVENT_READ, data=None)

    def accept_connection(self, key):  # Accept new connection and add it to selector
        sock, addr = key.fileobj.accept()
        sock.setblocking(False)
        print('\t* Accepted connection from:', addr)
        self.selector.register(
            sock,
            events=selectors.EVENT_READ | selectors.EVENT_WRITE,
            data=SocketData(addr)
        )

    def __close_connection(self, sock: Union[socket.socket, Any]):  # Close connection and remove it from selector
        print('\t* Closing connection from:', sock.getpeername())
        self.selector.unregister(sock)
        sock.close()

    @staticmethod
    def __finish_response(data: SocketData):  # Finish response and create it's stream
        set_request_headers(data.response, data.request)
        set_default_headers(data.response)

        streamer = streamer_selector(data.response, data.request)
        data.response_stream = streamer(data.response).stream()

    def service_connection(self, key, mask):
        sock = key.fileobj
        data: SocketData = key.data

        if mask & selectors.EVENT_READ and not data.request.finished:  # Read data and parse request
            if received_data := sock.recv(1024):
                data.request_data += received_data
                data.request_parser.feed_data(received_data)

        if data.request.finished and not data.response_stream:  # Handle the request
            path = data.request.url.path
            file_path = Path().absolute() / 'static' / path[1:] if path and path != '/' else None

            if path == '/':
                data.response = TextResponse('👽Hello World!👽'.encode())
            elif path == '/500':
                raise Exception('Error test.')
            elif file_path and file_path.is_file():
                data.response = FileResponse(file_path.open('rb'))
            else:
                data.response = ErrorResponse(404)
            self.__finish_response(data)

            print()
            print('-->', data.request)
            print('<--', data.response)
            print()

        if mask & selectors.EVENT_WRITE and data.response_stream:  # Stream response
            if (response_chunk := next(data.response_stream, None)) is not None:
                sock.send(response_chunk)  # Send chunk from response stream
            else:
                data.clear()  # Clear socket data
                if not (data.request.keep_alive or data.response.keep_alive):  # No keep-alive, no connection
                    self.__close_connection(sock)

    def loop(self):
        for key, mask in self.selector.select(timeout=0.5):
            try:
                if key.data:
                    self.service_connection(key, mask)  # Service existing connections
                else:
                    self.accept_connection(key)  # Accept new connection
            except (ConnectionAbortedError, ConnectionResetError):  # Close connection on abortion
                self.__close_connection(key.fileobj)
            except (HttpParserError, Exception) as exc:  # Handle exceptions and send error responses
                print_exc()
                key.data.response = ErrorResponse(
                    400 if type(exc) is HttpParserError or issubclass(type(exc), HttpParserError) else 500
                )
                key.data.response.write_exception(exc)
                self.__finish_response(key.data)

    def loop_forever(self):  # Run loop forever
        self.running = True
        while self.running:
            self.loop()

    def close(self):  # Safely close everything
        print('\t* Killing the server')
        self.running = False
        self.selector.close()
        self.listener_sock.close()


if __name__ == '__main__':
    s = Server()
    try:
        s.loop_forever()
    except KeyboardInterrupt:
        s.close()
